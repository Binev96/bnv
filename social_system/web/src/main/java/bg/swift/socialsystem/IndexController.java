package bg.swift.socialsystem;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class IndexController {

    private final PersonsRepository personRepository;

    public IndexController(PersonsRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping("/")
    public String getIndexWithAllPeople(Model model) {

        Iterable<Person> people = personRepository.findAll();

        model.addAttribute("people", people);

        return "index";
    }

}
