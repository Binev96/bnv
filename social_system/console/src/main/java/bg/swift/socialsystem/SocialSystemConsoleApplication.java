package bg.swift.socialsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocialSystemConsoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocialSystemConsoleApplication.class, args);
    }
}
