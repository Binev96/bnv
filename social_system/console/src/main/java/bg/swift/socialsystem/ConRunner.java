package bg.swift.socialsystem;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

@Service
public class ConRunner implements ApplicationRunner {

    PersonService personService;

    ConRunner(PersonService repository) {
        this.personService = repository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Scanner sc = new Scanner(System.in);
        while (true) {
            String firstCmd = sc.nextLine();
            String[] count = firstCmd.split(" ");
            if ("end".equalsIgnoreCase(firstCmd)) {
                return;
            }
            if ("all".equalsIgnoreCase(firstCmd)) {
                personService.printAllPeople();
            }

            if ("add".equalsIgnoreCase(count[0])) {
                for(int i = 0; i < Integer.parseInt(count[1]);i++) {
                    String lineOne = sc.nextLine();

                    String lineTwo = sc.nextLine();

                    String lineTree = sc.nextLine();

                    String[] cmdF = lineOne.split(";", -1);


                    String firstName = cmdF[0];
                    String lastName = cmdF[1];
                    char gender = cmdF[2].charAt(0);
                    Double height = 0.0;
                    if (!cmdF[3].trim().equals("")) {
                        height = Double.parseDouble(cmdF[3]);
                    }
                    String dOB = cmdF[4];
                    String country = cmdF[5];
                    String city = cmdF[6];
                    String municipality = cmdF[7];
                    int postcode = Integer.parseInt(cmdF[8]);
                    String streetName = cmdF[9];
                    int streetNumber = Integer.parseInt(cmdF[10]);

                    List<Education> educations = educations(lineTwo);
                    List<Insurance> insurances = insurances(lineTree);

                    Person person = new Person(firstName, lastName, gender, height, dOB, country, city, municipality, postcode, streetName, streetNumber, educations, insurances);

                    personService.addPerson(person);
                }
                System.out.println("Record added.");
            }

        }

    }
    List<Education> educations(String line) {
        List<Education> educations = new ArrayList<>();
        // if the line is empty, that means that no educations are present for the given person.
        if (line.isEmpty()) {
            return Collections.emptyList();
        }

        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy");
        String[] split = line.split(";", -1);
        for (int i = 0; i < split.length; i += 6) {

            char educationType = split[i].charAt(0);
            String institution = split[i + 1];
            String enrollmentDate = split[i + 2];
            String graduationDate = split[i + 3];
            String hasGraduated = split[i + 4];
            Double grade = parseGrade(split[i + 5]);

            Education education = new Education(educationType, institution, enrollmentDate, graduationDate, hasGraduated, grade);
            educations.add(education);

        }
        return educations;
    }

    List<Insurance> insurances(String line) {
        List<Insurance> insurances = new ArrayList<>();
        // if the line is empty, that means that no insurances are present for the given person.
        if (line.isEmpty()) {
            return Collections.emptyList();
        }

        String[] split = line.split(";", -1);
        for (int i = 0; i < split.length; i += 3) {

            Double amount = Double.parseDouble(split[i]);
            String year = split[i + 1];
            String month = split[i + 2];

            Insurance insurance = new Insurance(amount, year, month);
            insurances.add(insurance);

        }
        return insurances;
    }
    private static Double parseGrade(String grade) {

        if (grade == null || grade.isEmpty()) {
            return null;
        }

        return Double.parseDouble(grade);
    }
}

