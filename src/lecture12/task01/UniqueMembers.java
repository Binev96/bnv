package lecture12.task01;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class UniqueMembers {

    List<Integer> uniqueNumbersFrom(List<Integer> input){
        Set<Integer> set =  new LinkedHashSet<>(input);
        List<Integer> answer = new ArrayList<>(set);
        return answer;
    }

}
