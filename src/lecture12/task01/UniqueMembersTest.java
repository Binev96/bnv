package lecture12.task01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UniqueMembersTest {

//| Входен списък:    | Очакван изход:   |
//            |----------|---------|
//            | [1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3] | [1, 2, 3] |
//            | [1, 1, 1, 1, 1] | [1] |
//            | [1, 2, 3] | [1, 2, 3] |
//            | [] | [] |

    @Test
    public void empty_input_returns_empty_output(){
        //GIVEN
        UniqueMembers uniqueMembers = new UniqueMembers();

        //WHEN
        List<Integer> output = uniqueMembers.uniqueNumbersFrom(List.of(1,1,1,1,1,1,1));
        //THEN
        Assertions.assertEquals(1, output.size());
        Assertions.assertEquals(1, (int) output.get(0));
    }

    @Test
    public void repeating_elements_that_only_counter_one(){
        //GIVEN
        UniqueMembers uniqueMembers = new UniqueMembers();

        //WHEN
        List<Integer> output = uniqueMembers.uniqueNumbersFrom(List.of(1,2,3,1,2,3,1,2,3));
        //THEN
        Assertions.assertEquals(3, output.size());
        Assertions.assertEquals(1, (int) output.get(0));
        Assertions.assertEquals(2, (int) output.get(1));
        Assertions.assertEquals(3, (int) output.get(2));
    }

}