package lecture08.banktask.bank;

import lecture08.banktask.Credentials;

public class Account {
    Credentials credentials;
    double balance;
    private double[] lastTransactions;
    private int lastTransactionsId;

    Account(Credentials credentials) {
        this.credentials = credentials;
        this.lastTransactions = new double[10];
        this.lastTransactionsId = 0;
    }

    void debit(double amount) {
        if (amount < 0 || amount < this.balance) {
            return;
        }
        this.balance = this.balance - amount;
        addTransaction(-amount);
    }

    void credit(double amount) {
        if (amount < 0) {
            return;
        }
        this.balance += amount;
        addTransaction(amount);
    }

    private void addTransaction(double amount) {
        for (int i = lastTransactionsId - 1; i > 0; i--) {
            lastTransactions[i] = lastTransactions[i - 1];
        }

        lastTransactions[0] = amount;

        if (lastTransactionsId < 10) {
            lastTransactionsId++;
        }
    }
}
