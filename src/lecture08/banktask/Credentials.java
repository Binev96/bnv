package lecture08.banktask;

public class Credentials {
    String username;
    String password;
    Credentials(String username, String password){
        this.username = username;
        this.password = password;
    }
    boolean match(Credentials credentials){
        return this.username.equals(credentials.username) && this.password.equals(credentials.password);
    }
}
