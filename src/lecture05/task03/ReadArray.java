package lecture05.task03;

import java.util.Scanner;

public class ReadArray {
    public static void main(String[] args) {
        //Напишете програма lecture05.task03.ReadArray, която да създава масив от тип int и да го инициализира със стойности, въведени от стандартния вход.
        //На първия ред на стандартния вход ще бъде въведено число N, което ще указва броя елементи, които ще бъдат въведени след това.
        //На втория ред стандартния вход ще бъдат въведени N на брой числа, разделени с интервал. Нека създаденият масив да има големина точно N.
        //Отпечатайте въведените числа, разделени със запетая, на стандартния изход.
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        for(int i = 1; i<=input;i++){
            System.out.print(i);
            if(i!=input) {
                System.out.print(", ");
            }
        }
    }
}
