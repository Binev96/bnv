package lecture05.task01;

public class CreateArray {
    public static void main(String[] args) {
        //Напишете програма lecture05.task01.CreateArray, която да създава масив от тип int и да го инициализира със стойностите 5, 9, 11, 3, 6, 4, 7.
        //Отпечатайте стойностите на екрана, всяка на нов ред.
        int[] numbers = {5, 9, 11, 3, 6, 4, 7};
        for(int number : numbers){
            System.out.println(number);
        }
    }
}
