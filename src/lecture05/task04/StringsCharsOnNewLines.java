package lecture05.task04;
import java.util.Scanner;
public class StringsCharsOnNewLines {
    public static void main(String[] args) {
//        Напишете програма lecture05.task04.StringsCharsOnNewLines, която да чете от стандартния вход един символен низ и да го отпечата на екрана,
//         като всяка буква е на нов ред.
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        for(int i=0; i < line.length();i++){
            System.out.println(line.charAt(i));
        }
    }
    
}
