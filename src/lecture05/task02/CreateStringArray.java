package lecture05.task02;

public class CreateStringArray {
    public static void main(String[] args) {
        //Напишете програма lecture05.task02.CreateStringArray, която да създава масив от тип String и да го инициализира с 4 низа - имена на хора.
        //Програмата да разпечатва на нов ред всеки един от елементите на масива.
        String[] names = {"Ivan", "Dragan", "Petkan", "Sean", "Oggie"};
        for(String name: names){
            System.out.println(name);
        }
    }
}
