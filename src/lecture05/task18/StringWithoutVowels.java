package lecture05.task18;

import java.util.Scanner;

public class StringWithoutVowels {
    public static void main(String[] args) {
        do {
            char[] vowels = {'a', 'o', 'e', 'i', 'u', 'A', 'O', 'E', 'I', 'U'};
            Scanner sc = new Scanner(System.in);
            System.out.println("Input string:");
            String exercise = sc.nextLine();
            String space = "";
            for (int i = 0; i < vowels.length; i++) {

                for (int j = 0; j < exercise.length(); j++) {
                    if (exercise.charAt(j) != vowels[i]) {
                        space += exercise.charAt(j);
                    }
                    //  System.out.println(space);
                }
                exercise = space;
                space = "";
            }
            System.out.println(exercise);

        }
        while (true);
    }
}