package lecture05.task06;

import java.util.Scanner;

public class Print2dArrayUpAndDownWalk {
    public static void main(String[] args) {
        // Напишете програма lecture05.task06.Print2dArrayUpAndDownWalk, която да чете от стандартния вход едно число N и да отпечатва матрица NxN
        // с числата от 1 до N*N, следвайки формата:
        Scanner sc = new Scanner(System.in);
        int input = 4;
       // int c = 1;
        for (int i = 1; i <= input; i++) {
            System.out.print(i);
            for (int j = 1; j <= input; j++) {
                System.out.print('j');
                if(j%4==0) {
                    System.out.println();
                }
               // c++;
            }
        }
    }
}