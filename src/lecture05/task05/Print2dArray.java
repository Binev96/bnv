package lecture05.task05;

import java.util.Scanner;

public class Print2dArray {
    public static void main(String[] args) {
        // Напишете програма lecture05.task05.Print2dArray
        // която да чете от стандартния вход едно число N и да отпечатва матрица NxN с числата от 1 до N*N, следвайки формата:
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        for (int i = 1; i <= (input * input); i++) {
            System.out.print(i + " ");
            if (i % 4 == 0) {
                System.out.println();
            }
        }
        /**
         * 2 loops
         */
        /*
                Scanner sc = new Scanner(System.in);
        int input = 4;
        int c = 1;
        for (int i = 1; i <= input; i++) {
            for (int j = 1; j <= input; j++) {
                System.out.print(c);
                if(c%4==0){
                    System.out.println();
                }
                c++;
            }
         */
    }
}
