package lecture05.task17;

import java.util.Scanner;

public class IsPalindrome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input string:");
        String input = sc.nextLine();
        String reverse = "";
        for(int i = input.length()-1;i >= 0 ;i--){
            reverse += input.charAt(i);
        }
        if(input.equals(reverse)){
            System.out.println(input.equals(reverse));
        }else{
            System.out.println(input.equals(reverse));
        }
    }
}
