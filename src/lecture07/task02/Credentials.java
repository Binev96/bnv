package lecture07.task02;

public class Credentials {
    final String username;
    String password;
    String[] historicalPasswords = new String[100];
    int counter;
    boolean counterOver100;

    Credentials(String username, String password) {
        this.username = username;
        this.password = password;
        this.counter = 1;
        this.counterOver100 = false;
    }

    boolean isPassValid(String password) {
        return this.password.equals(password);
    }

    void changePassword(String newPassword, String oldPassword) {
        if (isPassValid(oldPassword)) {
            return;
        }
        this.password = newPassword;

        if (this.counter >= 100) {
            this.counter = 1;
            this.counterOver100 = true;
        }
        this.historicalPasswords[counter] = password;
        this.counter++;

    }

    boolean isPassswordAlreadyInputed(String password) {
        int loopFor;
        if (counterOver100) {
            loopFor = this.historicalPasswords.length;
        } else {
            loopFor = counter;
        }
        for (int i = 0; i < loopFor; i++) {
            if (this.historicalPasswords[i].equals(password)) {
                return true;
            }
        }
        return false;
    }
}