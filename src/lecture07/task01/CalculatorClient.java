package lecture07.task01;

import java.util.Scanner;

public class CalculatorClient {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        Calculator cl = new Calculator();
        String resault = "ERROR";
        while(!input.equals("END")){
            String[] arr = input.split(" ");
            double a = Double.parseDouble(arr[1]);
            double b = Double.parseDouble(arr[2]);
            if(arr[0].equals("SUM")){
                resault = ""+(cl.sum(a,b));
            }
            if(arr[0].equals("SUB")){
                resault = ""+(cl.substract(a,b));
            }
            if(arr[0].equals("MUL")){
                resault = ""+(cl.multiply(a,b));
            }
            if(arr[0].equals("DEV")){
                resault = ""+(cl.devide(a,b));
            }
            if(arr[0].equals("PER")){
                resault = ""+(cl.percantage(a,b));
            }

            System.out.printf("%.3f",Double.parseDouble(resault));
            input = sc.nextLine();
        }

    }
}
