package inclass;

import java.util.*;

public class collectionsPractice {
    public static void main(String[] args) {
        List<String> myList = List.of("Alice", "Bob", "charlie");

        Set<String> mySet = listToSet(myList);

        mySet.add("Alice");
        Map<String, Integer> phoneNumbers = new HashMap<>();
        List<Integer> numbers = List.of(123, 987, 567);
        addPhoneNumbers(phoneNumbers, mySet, numbers);
        System.out.println(phoneNumbers);

    }

    private static void addPhoneNumbers(Map<String, Integer> phoneNumbers, Set<String> names, List<Integer> numbers) {
        int counter = 0;
        for (String name : names) {
            phoneNumbers.put(name, numbers.get(counter));
            counter++;
        }
    }

    private static <E> Set<E> listToSet(List<E> List) {
        return new LinkedHashSet<>(List);
    }

    private static void printCollection(Collection<String> collection) {
        System.out.println(collection);
    }
}
