package lecture09.task01;

public class Person {
    String firstName;
    String lastName;

    public Person(String firstName, String lastName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    String getInfo() {
        return String.format("First name: %s \n" +
                "Last Name:%s \n",firstName, lastName );
    }
}