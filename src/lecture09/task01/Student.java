package lecture09.task01;

public class Student extends Person {
    int facultyNumber;
    int lectureCount;
    int exerciseCount;
    final String occupation = "Student";

    public Student(String firstName, String lastName, int facultyNumber, int lectureCount, int exerciseCount) {
    super(firstName, lastName);
    this.facultyNumber = facultyNumber;
    this.lectureCount = lectureCount;
    this.exerciseCount = exerciseCount;
    }
    @Override
    String getInfo(){
        String baseInfo = super.getInfo();
        return String.format(baseInfo +                "Occupation:%s%n" +
                "Faculty number:%d %n", occupation, facultyNumber );
    }
}
