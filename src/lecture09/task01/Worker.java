package lecture09.task01;

public class Worker extends Person {
    double weekSalary;
    double workHoursPerDay;
    final String occupation = "Worker";

    Worker(String firstName, String lastName, double weekSalary, double workHoursPerDay){
    super(firstName, lastName);
    this.weekSalary = weekSalary;
    this.workHoursPerDay = workHoursPerDay;
    }
    @Override
    String getInfo(){
        String baseInfo = super.getInfo();
        return String.format(baseInfo +
                "Occupation:%s%n" +
                "Week salary:%f%n" +
                "Hours per day:%f%n" +
                "Salary per hour:%f", occupation, weekSalary, workHoursPerDay, (weekSalary*workHoursPerDay) );
    }
}
