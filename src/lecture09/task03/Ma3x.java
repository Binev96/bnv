package lecture09.task03;

public class Ma3x {

    double pricePerHour;

    Ma3x(double pricePerHour){
        this.pricePerHour = pricePerHour;
    }

    double charge(Customer customer, int hours){
    return (pricePerHour*hours)-((pricePerHour*hours)*customer.getDiscount())/100;
    }
}
