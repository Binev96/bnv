package lecture09.task03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Ma3xTest {

    @Test
    void silver_customer_has_10_percent_discount() {
        // GIVEN
        Ma3x ma3x = new Ma3x(2);
        SilverVIP customer = new SilverVIP();

        // WHEN
        double price = ma3x.charge(customer, 2);

        //THEN
        Assertions.assertEquals(3.6, price);
    }
    @Test

    void gold_customer_has_25_percent_discount() {
        // GIVEN
        Ma3x ma3x = new Ma3x(2);
        GoldVIP customer = new GoldVIP();

        // WHEN
        double price = ma3x.charge(customer, 2);

        //THEN
        Assertions.assertEquals(3, price);
    }

}