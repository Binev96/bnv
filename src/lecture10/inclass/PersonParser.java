package lecture10.inclass;

import java.util.Scanner;

public class PersonParser {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int dateOfBirth = sc.nextInt();

        int age = validateAge(dateOfBirth);

        System.out.println("You are "+ age + " years old.");
    }

    private static int validateAge(int dateOfBirth) {
       /* if(dateOfBirth>2018){
            throw new YearOfBirthFromTheFutureException(dateOfBirth);
        }
        if(dateOfBirth < 1850){
            throw new YearOfBirthImpossiblyLongTimeAgoException();
        }
        */
        return 2018 - dateOfBirth;
    }
}
