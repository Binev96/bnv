package lecture10.inclass;

public class YearOfBirthFromTheFutureException extends RuntimeException {

    YearOfBirthFromTheFutureException(int yearOfBirth) {
        super("You have to be dead, you cant be born in" +  yearOfBirth);
    }

}
