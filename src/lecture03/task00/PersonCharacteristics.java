package lecture03.task00;

public class PersonCharacteristics {
    public static void main(String[] args) {
        String firstName = "Georgi";
        String lastName = "Binev";
        int birthDate = 1996;
        double weight = 78.6;
        double height = 177.7;
        String profession = "student";
        System.out.println(firstName+" "+lastName+" is "+(2018-birthDate)+" years old. His weight is "+weight+" kg and he is "+height+" cm tall. He is a "+profession+".");
    }
}
