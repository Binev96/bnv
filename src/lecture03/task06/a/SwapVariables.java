package lecture03.task06.a;
import java.util.Scanner;
public class SwapVariables {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Input nubmer for a:");
        int a = scanner.nextInt();
        System.out.print("Input nubmer for b:");
        int b = scanner.nextInt();
        System.out.println("Before:");
        System.out.println("a = "+a+" | b="+b);
        a = a+b;
        b= a-b;
        a= a-b;
        System.out.println("After:");
        System.out.println("a = "+a+" | b="+b);
    }
}
