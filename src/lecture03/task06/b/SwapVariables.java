package lecture03.task06.b;
import java.util.Scanner;
public class SwapVariables {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input nubmer for a:");
        int a = scanner.nextInt();
        System.out.print("Input nubmer for b:");
        int b = scanner.nextInt();
        int c;
        System.out.println("Before:");
        System.out.println("a = "+a+" | b="+b);
        c = a;
        a = b;
        b = c;
        System.out.println("After:");
        System.out.println("a = "+a+" | b="+b);
    }
}
