package lecture03.task08;
import java.util.Scanner;
public class LeapYear {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input year:");
        int year = scanner.nextInt();
        boolean isLeapYear = (year % 100 == 0) ? (year % 400 == 0) : (year % 4 == 0); // it is actually shortif :/ (if-statement) ? true / false
        System.out.println(isLeapYear);
    }
}
