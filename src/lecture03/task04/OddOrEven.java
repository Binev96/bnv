package lecture03.task04;

public class OddOrEven {
    public static void main(String[] args) {
        //Напишете програма lecture03.task04.OddOrEven, която да отпечатва дали въведено от конзолата число е четно или нечетно.
        int num = 2;
        if (num != (int)num) {
            System.out.println("It should be int.");
        } else {
            if (num % 2 == 0) {
                System.out.printf("The number %d is EVEN", num);
            } else {
                System.out.printf("The number %d is ODD", num);
            }
        }
    }
}
