package lecture03.task07;

import javax.swing.*;
import java.util.Scanner;

public class SecondsInBiggerIntervals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int minutesSeconds = 60;
        int hoursSeconds = minutesSeconds * 60;
        int daysSeconds = hoursSeconds * 24;
        int seconds, minutes, hours, days;
        System.out.println("Input seconds:");
        int inputSeconds = scanner.nextInt();
        if (inputSeconds < 0) {
            System.out.println("You can not input seconds below 0.");
        } else {
            days = inputSeconds / daysSeconds;
            hours = (inputSeconds % daysSeconds) / hoursSeconds;

            minutes = ((inputSeconds % daysSeconds) % hoursSeconds) / minutesSeconds;

            seconds = ((inputSeconds % daysSeconds) % hoursSeconds) % minutesSeconds;

            System.out.println("Days:" + days + " Hours:" + hours + " Minutes:" + minutes + " Seconds:" + seconds);
        }
    }
}
