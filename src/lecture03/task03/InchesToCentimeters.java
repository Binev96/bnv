package lecture03.task03;

import java.util.Scanner;

public class InchesToCentimeters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float inches = scanner.nextInt();
        System.out.println(inches*2.54+"cm.");
    }
}
