package bg.swift.lecture06.task07;

import java.util.Scanner;

public class EmployeeSalarySort {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int people = sc.nextInt();
        sc.nextLine();
        Employee[] employees = new Employee[people];
        for (int i = 0; i < people; i++) {
            String input = sc.nextLine();
            Employee employee = createEmployeFromInput(input);
            employees[i] = employee;
        }
        sortEmployeesBySalary(employees);
        printTopEmployees(employees);
    }

    private static void printTopEmployees(Employee[] employees) {

    }

    private static void sortEmployeesBySalary(Employee[] employees) {
        System.out.println(employees.length);

        for (int i = 0; i < employees.length; i++) {
            System.out.println(employees[i].salary);
        }
    }


    private static Employee createEmployeFromInput(String input) {
        //String name, double salary, String position, String department, int age, String email
        String[] inputFields = input.split(",");
        String name = inputFields[0];
        double salary = Double.valueOf(inputFields[1].trim());
        String position = inputFields[2];
        String department = inputFields[3];
        Employee employee = new Employee(name, salary, position, department);
        //int age = Integer.valueOf(inputFields[4]);
        if (inputFields.length >= 5) {
            employee.age = Integer.valueOf(inputFields[4].trim());
        }
        if (inputFields.length >= 6) {
            employee.email = inputFields[5];
        }

        return employee;
    }

}
