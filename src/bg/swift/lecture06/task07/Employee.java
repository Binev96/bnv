package bg.swift.lecture06.task07;

public class Employee {
    /*
    Създайте клас bg.swift.lecture06.task07.Employee, който съдържа задължителните характеристики име (name), заплата (salary), позиция (position),
     отдел (department) и незадължителните години (age) и електронна поща (email). Напишете подходящите полета, свойства и конструктори.
     Напишете програма bg.swift.lecture06.task07.EmployeeSalarySort, която чете от стандартния вход число N и след това N на брой реда във формата:
<name>,<salary>,<position>,<department>[,<age>][,<email>]
Програмата да изведе тримата работници с най-високи заплати, подредени в низходящ ред, като отпечата името, отдела, позицията и ако работника има електронна поща, също. Формата да бъде следния:
<name>, <department>, <position>[, <email>]
     */
    final String name;
    final double salary;
    int age;
    String email;
    final String position;
    final String department;

    Employee(String name, double salary, String position, String department){
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
    }
    Employee(String name, double salary, String position, String department, int age){
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.age = age;
    }
    Employee(String name, double salary, String position, String department, String email){
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.email = email;
    }
    Employee(String name, double salary, String position, String department, int age, String email){
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.age = age;
        this.email = email;
    }
}
