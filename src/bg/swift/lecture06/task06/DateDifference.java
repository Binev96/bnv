package bg.swift.lecture06.task06;

public class DateDifference {
    public static void main(String[] args) {
        SwiftDate firstDate = new SwiftDate(2018, 12, 15);
        SwiftDate firstDate2 = new SwiftDate(2222, 12, 15);
        System.out.println(firstDate.getInfo());
        System.out.println(firstDate2.getInfo());
    }
}