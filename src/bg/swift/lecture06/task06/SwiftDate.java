package bg.swift.lecture06.task06;

public class SwiftDate {
    int date, year, month = 0;

    boolean isLeapYear() {
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
            return true;
        } else {
            return false;
        }
    }

    int getCentury() {
        return (year/100)+1;
    }

    SwiftDate(int year, int month, int date) {
        this.year = year;
        this.month = month;
        this.date = date;
    }

    String getInfo() {
        String leapYearTemplate = "";
        if (isLeapYear()) {
            leapYearTemplate = "[ It is LEAP year.]";
        } else {
            leapYearTemplate = "[ It is NOT LEAP year.]";
        }
        //                              yyyy MM dd - XX century.
        String info = String.format("%d %d %d - %d century. %s", year, month, date, getCentury(), leapYearTemplate);
        return info;
    }
}
