package bg.swift.lecture03.task02;

public class AdultChecker {
    public static void main(String[] args) {
        /*
        Напишете програма lecture03.task02.AdultChecker, в която в променлива age от тип int прочетете от конзолата въведено число - години на човек.
        В променлива isAdult от тип boolean, използвайки операторите за сравнение, трябва да се запише резултатът от проверката дали годините са повече
        или по-малко от 18. Разпечатайте на екрана boolean променливата.
        */
        int age = -5;
        System.out.println(age);
        if(age<0){
        System.out.println("Error");
    }
        else {
        boolean isAdult = (age >= 18);
        System.out.println(isAdult);
    }
}
}
