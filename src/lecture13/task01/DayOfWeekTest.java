package lecture13.task01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DayOfWeekTest {

    @Test

    void sunday_not_work_day(){
        Assertions.assertFalse(DayOfWeek.SUNDAY.isWorkDay());
    }
    @Test

    void monday_work_day(){
        Assertions.assertTrue(DayOfWeek.MONDAY.isWorkDay());
    }

}