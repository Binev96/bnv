package lecture13.task01;

public enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    public boolean isWorkDay() {

        if (DayOfWeek.SUNDAY.equals(this) || DayOfWeek.SATURDAY.equals(this)) {
            return false;
        }
        return true;
    }
}

class Main {
    public static void main(String[] args) {
        if(DayOfWeek.TUESDAY.isWorkDay()){
            System.out.println("Raboten den. Работен ден.");
        }else{

            System.out.println("Pochiven den. Почивен ден.");
        }
    }
}
