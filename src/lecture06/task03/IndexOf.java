package lecture06.task03;

public class IndexOf {
    public static void main(String[] args) {
        int[] arr1 = {5, 6, 7, 9, 8};
        int[] arr2 = {1, 1, 1};

        int index1 = indexOf(arr1, 9);
        int index2 = indexOf(arr2, 2);

        System.out.println(index1);
        System.out.println(index2);
    }

    private static int indexOf(int[] arr, int num) {
        for(int i = 0; i < arr.length; i++){
            if(arr[i]==num){
                return i;
            }
        }
        return -1;
    }


}
