package lecture06.task05;

public class Main {
    public static void main(String[] args) {
        Car mercedes = new Car("Mercedes-Benz S220", 2009, 160);
        Car bmw = new Car("BMW e90 2.5Xi", 2006, 218);
        Car opel = new Car("Opel Astra", 1996, 85);
        Car bugatti = new Car("Mercedes-Benz S220", 2015, 612);
        Car lada = new Car("Lada 5", 1989, 75);

        System.out.println(mercedes.eligibleTax());
        System.out.println(bmw.eligibleTax());
        System.out.println(opel.eligibleTax());
        System.out.println(bugatti.eligibleTax());
        System.out.println(lada.eligibleTax());
    }
}
