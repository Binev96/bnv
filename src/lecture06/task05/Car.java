package lecture06.task05;

public class Car {
    String carName;
    int carAgeOfProduction;
    int carHP;
    private int currentYear = 2018;
    public Car(String carName, int carAgeOfProduction, int carHP){
        this.carAgeOfProduction = carAgeOfProduction;
        this.carName = carName;
        this.carHP = carHP;
    }
    private int insuranceCategory(){
        if(currentYear-this.carAgeOfProduction>=25){
            return 4;
        }
        if(currentYear-this.carAgeOfProduction>=15){
            return 3;
        }
        if(currentYear-this.carAgeOfProduction>=8) {
            return 2;
        }
        return 1;
    }

    public double eligibleTax(){
        double tax;
        double additional = 0;
        if(this.carHP<=80){
            additional = 0.2;
        }
        if(this.carHP>=140){
            additional = 0.45;
        }
        if(this.insuranceCategory()==1){
            tax = 150;
            return (tax+(tax*additional));
        }
        if(this.insuranceCategory()==2){
            tax = 200;
            return (tax+(tax*additional));
        }
        if(this.insuranceCategory()==3){
            tax = 300;
            return (tax+(tax*additional));
        }
            tax = 500;
            return (tax+(tax*additional));
    }
}
