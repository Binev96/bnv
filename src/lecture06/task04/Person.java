package lecture06.task04;

public class Person {
    String firstName;
    int age;

    public Person() {
        firstName = "No name";
        age = -1;
    }

    public Person(String name) {
        firstName = name;
        age = -1;
    }

    public Person(String name, int age) {
        firstName = name;
        if (!(age < 0 || age >= 150)) {
            this.age = age;
        } else {
            System.out.println("Age is not correct");
            this.age = -1;
        }
    }

    public int getAge() {
        return this.age;
    }
    public String getName(){
        return this.firstName;
    }
    String intoduction(){
        if(firstName.equals("No name")){
            return "I am John Doe";
        }
        if(age==-1) {
        return "Hello, I am "+firstName;
        }

        return "Hello, I am "+firstName+" and I am "+age+" years old.";
    }
}

