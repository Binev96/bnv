package lecture06.task04;

public class Main {
    public static void main(String[] args) {
        Person p1 = new Person();
        System.out.println(p1.intoduction());
        Person p2 = new Person("Ivan");
        System.out.println(p2.intoduction());
        Person p3 = new Person("Ivan", 16);
        System.out.println(p3.intoduction());


    }
}
