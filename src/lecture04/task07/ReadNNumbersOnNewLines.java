package lecture04.task07;

import java.util.Scanner;

public class ReadNNumbersOnNewLines {
    public static void main(String[] args) {
        //Напишете програма lecture04.task07.ReadNNumbersOnNewLines, която да прочита от първия ред на стандартния вход едно число,
        //след което на следващите толкова на брой реда да има по едно число.
        //Програмата да ги отпечатва на един ред, разделени с интервал, след приключване на въвеждането.
        System.out.print("Input number:");
        Scanner sc = new Scanner(System.in);
        int num;
        int input = sc.nextInt();
        String oneLineNumbers = "";
        for (num = 0; num < input; num++) {
            if (num > 0) {
                sc.nextLine();
            }
            oneLineNumbers += sc.nextInt() + " ";
        }
        System.out.println(oneLineNumbers);
    }
}
