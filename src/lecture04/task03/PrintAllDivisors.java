package lecture04.task03;

import java.util.Scanner;

public class PrintAllDivisors {
    public static void main(String[] args) {
        //Напишете програма lecture04.task03.PrintAllDivisors, която прочита едно число и отпечатва всичките му делители.
        System.out.print("Input number(greater than 0):");
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        if (input > 0) {
            for (int num = 1; num <= input; num++) {
                if (input % num == 0) {
                    System.out.println(num);
                }
            }
        }else{
            System.out.println("You number is 0 or below 0");
        }
    }
}
