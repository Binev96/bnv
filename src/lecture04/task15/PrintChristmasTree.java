package lecture04.task15;

import java.util.Scanner;

public class PrintChristmasTree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Type number:");
        int input = 0;
        do {
            input = sc.nextInt();
            if(input <=4){
                System.out.print("You have to input number above 4:");
            }
        }while(input <= 4);
        String fig = "*";
        String figPr = "*";
        String space = " ";
        String centerStar = "";
        String centerBase = "";

        int loopNumber = input - 2;
        for(int i = loopNumber; i>0; i--){
            for(int j = 1; j < i;j++){
                System.out.print(" ");
                if(j <= 1) {
                    centerStar += space;
                }
                if(j == 2){
                    centerBase = centerStar;
                }
            }
            System.out.println(figPr);
            figPr+=fig+fig;

        }
        System.out.println(centerStar+"*");
        System.out.println(centerBase+"***");
    }
}
