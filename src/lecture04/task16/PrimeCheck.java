package lecture04.task16;

import java.util.Scanner;

public class PrimeCheck {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int check = 0;
        System.out.print("Type number:");
        int input = 0;
        input = sc.nextInt();
        if (input % input == 0) {
            check++;
        }
        for (int i = 1; input > i; i++) {
            if (input % i == 0) {
                check++;
            }
        }
        boolean isPrime = (check <= 2);
        if (input == 1) {
            isPrime = false;
        }
        System.out.println(isPrime);

    }
}
