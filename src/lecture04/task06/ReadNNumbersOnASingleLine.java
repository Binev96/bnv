package lecture04.task06;

import java.util.Scanner;

public class ReadNNumbersOnASingleLine {
    public static void main(String[] args) {
        //Напишете програма lecture04.task06.ReadNNumbersOnASingleLine, която прочита едно число N и после N на брой числа на един ред, разделени с интервал.
        //Програмата да ги отпечата, всяко на нов ред.
        System.out.print("Input number:");
        Scanner sc = new Scanner(System.in);
        int num;
        int input = sc.nextInt();
        for(num=0;num<input;num++){
            System.out.println(sc.nextInt());
        }

    }
}
