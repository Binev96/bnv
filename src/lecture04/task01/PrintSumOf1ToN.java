package lecture04.task01;
import java.util.Scanner;

public class PrintSumOf1ToN {
    public static void main(String[] args) {
        //Напишете програма lecture04.task01.PrintSumOf1ToN, която прочита едно число и отпечатва сбора на числата от 1 до въведеното число.
        Scanner sc = new Scanner(System.in);
        int sum = 0;
        System.out.print("Input number:");
        int input = sc.nextInt();
        for(int num = 1; num<=input;num++){
            sum += num;
        }
        System.out.println(sum);
    }
}
