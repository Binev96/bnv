package lecture04.task12;

import java.util.Scanner;
/*
Напишете програма lecture04.task12.PrintFirstDigit, която прочита едно число и отпечатва първата му цифра.
Можете ли без да ползвате String?
 */

public class PrintFirstDigit {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int savedNumber = number;
        int firstNumber = 0;
        while(number>0){
            firstNumber = number%10;
            number = number/10;
        }
        System.out.println("Input: "+firstNumber);
    }
}
