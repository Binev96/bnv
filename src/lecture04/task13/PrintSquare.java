package lecture04.task13;

import java.util.Scanner;

public class PrintSquare {
    /*
    Напишете програма lecture04.task13.PrintSquare, която прочита едно число N. Програмата да отпечатва квадрат със страна N използвайки знака *.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        String figure = "* ";
        String figure2 = "* ";
        for (int i = 0; i < input; i++) {

            if (i == 0 || i == input-1) {

                for (int j = 0; j < input; j++) {
                    System.out.print(figure);
                }
                System.out.println();

            } else {
                    System.out.print(figure + " ");
                for (int k = 0; k < input; k++) {
                    System.out.print(" ");
                }
                System.out.println(figure);

            }
        }
    }
}
