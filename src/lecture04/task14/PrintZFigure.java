package lecture04.task14;

import java.util.Scanner;

public class PrintZFigure {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Type number:");
        int input = sc.nextInt();
        for (int i = 0; i < input; i++) {
            if (i == 0 || i == input-1) {
                for (int j = 0; j < input; j++) {
                    if(j >= input) {
                        System.out.print("*");
                    }else{
                        System.out.print("* ");
                    }
                }
                System.out.println();
            } else {
                for (int j = input; i<j;j--){
                    if(i<j-2) {
                        System.out.print("  ");
                    }else{
                        System.out.print(" ");
                    }
                }
                System.out.println("*");
            }
        }
    }
}