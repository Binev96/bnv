package lecture04.task02;

import java.util.Scanner;

public class PrintNonDivisors {
    public static void main(String[] args) {
        /*
        Напишете програма lecture04.task02.PrintNonDivisors, която прочита едно число и отпечатва всички числа от 1 до въведеното число,
        които не се делят на 3 или 7 (имат остатък различен от 0 при деление с 3 и 7).*/
            System.out.print("Input number:");
            Scanner sc = new Scanner(System.in);
            int input = sc.nextInt();
            for(int num = 1; num<=input;num++){
                if(num%3!=0 && num%7!=0) {
                    System.out.println(num);
                }
            }

        }
}
