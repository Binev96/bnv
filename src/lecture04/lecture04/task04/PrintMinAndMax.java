package lecture04.lecture04.task04;

import java.util.Scanner;

public class PrintMinAndMax {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input number (above 0):");
        int input = sc.nextInt();
        int max = 0;
        int min = 0;
        for(int i=0; i<input;i++) {
            int number = sc.nextInt();
            if(i==0){
                min =number;
                max = number;
            }
            if (min > number) {
                min = number;
            }
            if (max < number) {
                max = number;
            }
        }
        System.out.println("Min:"+min+" Max:"+max);
    }
}
