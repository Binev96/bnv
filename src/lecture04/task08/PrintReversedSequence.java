package lecture04.task08;

import java.util.Scanner;

public class PrintReversedSequence {
    public static void main(String[] args) {
        //Напишете програма lecture04.task08.PrintReversedSequence, която прочита едно число N и после N на брой числа на един ред.
        //Програмата да отпечатва въведените числа в обратен ред след приключване на въвеждането.
        System.out.print("Input number:");
        Scanner sc = new Scanner(System.in);
        int num;
        int input = sc.nextInt();
        String oneLineNumbers = "";
        for(num=0;num<input;num++){
            oneLineNumbers = sc.nextInt()+" "+oneLineNumbers;
        }
        System.out.println(oneLineNumbers);
    }
}
