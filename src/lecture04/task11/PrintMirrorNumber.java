package lecture04.task11;

import java.util.Scanner;
//Напишете програма lecture04.task11.PrintMirrorNumber, която прочита едно число и отпечатва огледалното му.
// Когато не се ползва string ако числото е 120 нулата няма да бъде изписана !!!
public class PrintMirrorNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt(); //           Example: 34
        int savedNumber = number;
        int mirror = 0;
        while(number>0){
            mirror = mirror * 10; //            1st loop: 0                | 2nd loop: 4*10 = 40
            mirror = mirror + number%10;//      1st loop 0+(34%10=4) = 4   | 2nd loop: 40+(3%10=3) => 43
            number = number/10;//               1st loop 3.4(int 3)        | 2nd loop: 0.3(int 0)
        }
        System.out.println("Input: "+savedNumber);
        System.out.println("Mirrored: "+mirror);
    }
}
