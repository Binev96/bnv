package lecture04.task05;

import java.util.Scanner;

public class PrintSumOfN {
    public static void main(String[] args) {
        //Напишете програма lecture04.task05.PrintSumOfN, която прочита едно число N и после N на брой числа на един ред, разделени с интервал.
        // Програмата да отпечатва сбора на въведените числа след приключване на въвеждането.
        System.out.print("Input number:");
        Scanner sc = new Scanner(System.in);
        int num = 0;
        int input = sc.nextInt();
        int sum = 0;
        do {
            sum += sc.nextInt();
        num++;
        }while(input>num);
        System.out.println(sum);
    }
}


