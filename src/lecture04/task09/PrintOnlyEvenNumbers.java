package lecture04.task09;

import java.util.Scanner;

public class PrintOnlyEvenNumbers {
    public static void main(String[] args) {
        //Напишете програма lecture04.task09.PrintOnlyEvenNumbers, която прочита едно число N и после N на брой числа на един ред.
        //Програмата да отпечатва само четните от въведените числа след приключване на въвеждането.
        System.out.print("Input number:");
        Scanner sc = new Scanner(System.in);
        int num;
        int input = sc.nextInt();
        String oneLineNumbers = "";
        for(num=0;num<input;num++){
            if(num%2!=0) {
                oneLineNumbers += sc.nextInt() + " ";
            }
        }
        System.out.println(oneLineNumbers);
    }
}
