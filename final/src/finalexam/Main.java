package finalexam;

import finalexam.forum.User;

public class Main {
    public static void main(String[] args) {
        User alice = new User("Alice", "123456", "alice@forum.bg", "14.02.2019", "21:21", 3);
        User bob = new User("Bob", "123456", "alice@forum.bg", "14.02.2019", "11:41", 1);
        User charlie = new User("Charlie", "123456", "alice@forum.bg", "14.02.2019", "23:21", 1);
        bob.createTopic("First topic", "First msg.");
        alice.createPost("Second msg", "14.02.2019", "First topic");

    }
}
