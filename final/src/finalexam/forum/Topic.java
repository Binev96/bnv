package finalexam.forum;

public class Topic {
    private String topicName;
    private String topicCreator;
    private Boolean isLocked = false;
    private String reply;

    Topic(String name, String creator, String msg) {
        this.topicName = name;
        this.topicCreator = creator;
        this.reply = msg;
    }

    void lockTopic(int topic_id, String byWho) {
    }

    String getTopicName() {
        return this.topicName;
    }
    boolean isLocked(){
        return isLocked;
    }
    void changeName(String byWho, String topicName, int usertype) {//type = 3 is admin
        if (this.topicCreator.equalsIgnoreCase(byWho) || usertype == 3) {
            this.topicName = topicName;
        } else {
            System.out.println("Error.");
        }
    }
}
